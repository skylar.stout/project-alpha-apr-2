from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from tasks.forms import CreateTaskForm

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            create_task = form.save()
            create_task.owner = request.user
            create_task.save()
            return redirect("/projects/")

    else:
        form = CreateTaskForm()
        context = {
            "form": form,
        }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    if request.user.is_anonymous:
        return redirect("/projects/")

    context = {
        "show_my_tasks": Task.objects.filter(assignee=request.user.id),
    }
    return render(request, "tasks/show_my_tasks.html", context)
