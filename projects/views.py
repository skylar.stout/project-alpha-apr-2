from django.shortcuts import render, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    tasks = get_object_or_404(Project, id=id)
    context = {
        "tasks": tasks,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            create_project = form.save(False)
            create_project.owner = request.user
            create_project.save()
            return HttpResponseRedirect(reverse("list_projects"))
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
